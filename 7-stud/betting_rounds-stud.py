
# WOULD BE NICE TO SPLIT THESE OFF INTO A FILE, FOR SOME REASON GAME.PY COULDNT RECOGNIZE BETTING_ROUNDS.Betting_Rounds().BETTING_ROUND_PREFLOP

# IF USING THIS FILE, MUST BRING IN UPDATED FUNCTIONS IN GAME.PY

import time

#import deck
#import pokertable
#import players
#import dealer
#import rules
import organize_pots

class Ranks():
    suits_dict = {1: 'c', 2: 'd', 3: 'h', 4: 's'}
    ranks_dict = {1: '2', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9', 10: '10', 11: 'J', 12: 'Q', 13: 'K', 14: 'A'}



class Betting_Rounds():
#forced bet is like big blind, what everyone pays. after the flop, forced bet is 0, but there's still a minimum bet (usually the big blind, or 2x BB)
    @staticmethod
    def betting_round_preflop(players, forced_bet, minimum_bet, current_pots, big_blind):

        pots = current_pots

        #saved_players_list = deepcopy(players)

        big_blind_checked = False

        amount_of_players_remaining = len(players)

        #pot_counter = 0
        #print '\nBeginning of "betting_round"...'
        #for pot in pots:
            #print 'Pot %d:' % (pot_counter + 1)
            #print 'Pot size: %d' % pots[pot_counter]['pot']
            #print 'Bet to match for this pot: %d' % pots[pot_counter]['match']
            #print 'Any all in? %s' % pots[pot_counter]['any_all_in']
            #pot_counter += 1
        
        pots = current_pots
        players_list = players
        print '\nstarting betting round!!!'
        current_bet = forced_bet
        raise_amount = 0
        required_raise = 0
        if minimum_bet > 0 and forced_bet == 0:
            required_raise = minimum_bet
        else:
            required_raise = 2 * forced_bet
         
        old_bet = 0

        while True:
            for index, player in enumerate(players):
                if player.chips == 0:
                    player.all_in = True

                owe = current_bet - player.current_bet
                if owe == 0 and player.checked == True:
                    continue

                if player.has_hand == False:
                    continue
                

                print ('\nOn %s, %d chips' % (player.screen_name, player.chips))
                print ('Your cards are: %s%s, %s%s' % (Ranks().ranks_dict[player.cards[0].Rank], Ranks().suits_dict[player.cards[0].Suit], Ranks().ranks_dict[player.cards[1].Rank], Ranks().suits_dict[player.cards[1].Suit]))
                print 'Is all in? : %s' % player.all_in
                print 'Current bet: %s' % player.current_bet
                print 'Current investment: %s' % player.current_investment
                print 'Has hand? : %s' % player.has_hand
                print 'Checked? : %s' % player.checked
                print 'The current bet is %d' % current_bet
                print ('The current raise is: %d' % raise_amount)
                print ('The required raise is: %d' % required_raise)
                print 'Your current bet is %d' % player.current_bet
                print 'You owe: %d' % owe
                print 'You can bet up to: %d' % (player.current_bet + player.chips)
                print '\n'
                pot_counter = 0
                for pot in pots:
                    print 'Pot %d:' % (pot_counter + 1)
                    print 'Pot size: %d' % pots[pot_counter]['pot']
                    print 'Bet to match for this pot: %d' % pots[pot_counter]['match']
                    print 'Any all in? %s' % pots[pot_counter]['any_all_in']
                    pot_counter += 1
 
                calling_all_in = False
                while True:
                    if current_bet > 0:
                        if player.chips + player.current_bet <= current_bet:
                            action = raw_input("Do you want to call all in? y or n: ")
                            if action == 'y' or action == 'yes':
                                calling_all_in = True
                                break
                            elif action == 'n' or action == 'no':
                                player.has_hand = False
                                player.cards = []
                                player.current_bet = 0
                                break
                            else:
                                continue
                    break
                
                went_through_big_blind_option_code = False
                if player.has_hand == True:
                    while True:
                        if calling_all_in == True:
                            player_bet = player.chips + player.current_bet
                            print 'ALLL INNN!!!!!!!!!!!!!'
                            pots = organize_pots.Organize_Pots().organize_pots(player_bet, player, pots, players_list)
                            break
                        if current_bet == big_blind and player.big_blind == True:
                            print 'big blund player current bet: %d' % player.current_bet
                            player_bet = raw_input("\nYou can bet an integer,\ncheck your big blind with 'c' or 'check',\nor fold if you\'re an idiot: ")
                            went_through_big_blind_option_code = True
                            print 'player bet: %s' % player_bet
                            if player_bet == 'c' or player_bet == 'check':
                                player.checked = True
                                big_blind_checked = True
                                print ('\nYou decided to check your option\n')
                                break
                            test_bet = int(player_bet)
                            if test_bet == big_blind:
                                print 'what the hell'
                                player.checked = True
                                big_blind_checked = True
                                break

                        if went_through_big_blind_option_code == False:
                            player_bet = raw_input("\nYou can bet an integer,\nsay 'f' or 'fold' to fold,\nor say 'c' or 'check' to check, if the current bet is 0: ")
                        if player_bet == 'f' or player_bet == 'fold':
                            player.has_hand = False
                            player.cards = []
                            player.current_bet = 0
                            print 'You decided to fold'
                            break
                        if current_bet == 0 and player_bet == 'c' or current_bet == 0 and player_bet == 'check':
                            print ('\nYou decided to check\n')
                            player.checked = True
                            break
                        try:
                            bet = int(player_bet)
                        except ValueError:
                            print ('Enter an integer.')
                            continue
                        #if bet > current_bet and not bet >= (current_bet + (2 * current_raise)):
                        if (player.chips + player.current_bet - bet) == 0:
                            print ('ALLLLLLLL INNNNNNNN!!!!!!!!!!')
                            if bet >= required_raise:
                                old_bet = current_bet
                                raise_amount = bet - old_bet
                                pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                                current_raise = (2 * (bet - current_bet) + current_bet)
                                required_raise = ((2 * raise_amount) + old_bet)
                                print 'required raise: %d' % required_raise
                                current_bet = bet
                                print('%s chips: %d' % (player.screen_name, player.chips))
                                break
                            elif bet < required_raise:
                                old_bet = current_bet
                                pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                                current_bet = bet
                                print('%s chips: %d' % (player.screen_name, player.chips))
                                break
                        elif bet > current_bet and not bet >= required_raise:
                            print ('If you\'d like to raise, enter a number at least twice the current bet.')
                            continue
                        elif current_bet > bet:
                            print ('That is smaller than the current bet. Stop cheating, you cheapskate!!!')
                            continue
                        elif bet > player.chips:
                            print ('You don\'t have enough chips to bet that much!')
                            continue
                        elif current_bet == 0 and bet == 0:
                            print ('\nYou decided to check\n')
                            player.checked = True
                            break
                        elif bet == current_bet:
                            pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                            print ('%s chips: %d' % (player.screen_name, player.chips))
                            break
                        else:
                            old_bet = current_bet
                            raise_amount = bet - old_bet
                            #current_raise = (2 * (bet - current_bet) + current_bet)
                            required_raise = ((2 * raise_amount) + old_bet)
                            pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                            print 'required raise: %d' % required_raise
                            current_bet = bet
                            print('%s chips: %d' % (player.screen_name, player.chips))
                            break
                print 'current bet: %d' % current_bet
                print "\nmade it to left_to_act code\n"
                #end the game with a winner
                left_to_act = 0
                players_with_hands = 0
                all_in_players = []
                remaining_players = []
                    
                for player in players:
                    #print player.screen_name
                    if player.has_hand == True:
                        players_with_hands += 1
                        remaining_players.append(player)
                        if player.all_in == True:
                            all_in_players.append(player)
                            continue
                        if player.current_bet < current_bet:
                            left_to_act += 1
                            #print 'who are we adding? 1: %s' % player.screen_name
                        if current_bet == big_blind and player.current_bet == current_bet and player.big_blind == True and player.checked == False:
                            left_to_act += 1
                            #print 'big_blind_checked: %s' % big_blind_checked
                            #print 'who are we adding? 2: %s' % player.screen_name
                        if current_bet == 0:
                            if player.current_bet == 0:
                                if not player.checked:
                                    left_to_act += 1
                                    #print 'who are we adding? 3: %s' % player.screen_name

                

                #print "players left to act: %s" % left_to_act
                #print "players with hands: %s" % players_with_hands
                if players_with_hands == 1:
                    winner = []
                    for player in players:
                        if player.has_hand == True:
                            winner.append(player)
                    return ({'hand_over': True}, pots, winner)
                if players_with_hands == len(all_in_players):
                    return ({'hand_over': False}, pots, all_in_players, {'errbody_all_in': True})
                if left_to_act == 0:
                    print "\nround is over\n"
                    pot_counter = 0
                    print '\nEnding of "betting_round"...'
                    #for pot in pots:
                        #print 'why not go here? 4'
                        #print 'Pot %d:' % (pot_counter + 1)
                        #print 'Pot size: %d' % pots[pot_counter]['pot']
                        #print 'Bet to match for this pot: %d' % pots[pot_counter]['match']
                        #print 'Any all in? %s' % pots[pot_counter]['any_all_in']
                        #pot_counter += 1
                    return ({'hand_over': False}, pots, remaining_players, {'errbody_all_in': False})






    #forced bet is like big blind, what everyone pays. after the flop, forced bet is 0, but there's still a minimum bet (usually the big blind, or 2x BB)
    @staticmethod
    def betting_round_postflop(players, forced_bet, minimum_bet, current_pots, big_blind, board):

        pots = current_pots

        #saved_players_list = deepcopy(players)

        amount_of_players_remaining = len(players)

        #pot_counter = 0
        #print '\nBeginning of "betting_round"...'
        #for pot in pots:
            #print 'Pot %d:' % (pot_counter + 1)
            #print 'Pot size: %d' % pots[pot_counter]['pot']
            #print 'Bet to match for this pot: %d' % pots[pot_counter]['match']
            #print 'Any all in? %s' % pots[pot_counter]['any_all_in']
            #pot_counter += 1
        
        pots = current_pots
        players_list = players
        print '\nstarting betting round!!!'
        current_bet = forced_bet
        raise_amount = 0
        required_raise = 0
        if minimum_bet > 0 and forced_bet == 0:
            required_raise = minimum_bet
        else:
            required_raise = 2 * forced_bet
         
        old_bet = 0
                
        while True:       
            for index, player in enumerate(players):

                if player.chips == 0:
                    player.all_in = True

                owe = current_bet - player.current_bet
                if owe == 0 and player.checked == True:
                    continue

                if player.has_hand == False:
                    continue

                if player.all_in == True:
                    continue


                card_sub = ''
                count = 0
                for card in board:
                    card_sub += Ranks().ranks_dict[board[count].Rank]
                    card_sub += Ranks().suits_dict[board[count].Suit]
                    card_sub +=' '
                    count += 1
                print '\nBoard: %s' % card_sub


                print ('\nOn %s, %d chips' % (player.screen_name, player.chips))
                print ('Your cards are: %s%s, %s%s' % (Ranks().ranks_dict[player.cards[0].Rank], Ranks().suits_dict[player.cards[0].Suit], Ranks().ranks_dict[player.cards[1].Rank], Ranks().suits_dict[player.cards[1].Suit]))
                print 'Is all in? : %s' % player.all_in
                print 'Current bet: %s' % player.current_bet
                print 'Current investment: %s' % player.current_investment
                print 'Has hand? : %s' % player.has_hand
                print 'Checked? : %s' % player.checked
                print 'The current bet is %d' % current_bet
                print ('The current raise is: %d' % raise_amount)
                print ('The required raise is: %d' % required_raise)
                print 'Your current bet is %d' % player.current_bet
                print 'You owe: %d' % owe
                print 'You can bet up to: %d' % (player.current_bet + player.chips)
                print '\n'
                pot_counter = 0
                for pot in pots:
                    print 'Pot %d:' % (pot_counter + 1)
                    print 'Pot size: %d' % pots[pot_counter]['pot']
                    print 'Bet to match for this pot: %d' % pots[pot_counter]['match']
                    print 'Any all in? %s' % pots[pot_counter]['any_all_in']
                    pot_counter += 1
 
                calling_all_in = False
                while True:
                    if current_bet > 0:
                        if player.chips + player.current_bet <= current_bet:
                            action = raw_input("Do you want to call all in? y or n: ")
                            if action == 'y' or action == 'yes':
                                calling_all_in = True
                                break
                            elif action == 'n' or action == 'no':
                                player.has_hand = False
                                player.cards = []
                                player.current_bet = 0
                                break
                            else:
                                continue
                    break
                
                went_through_big_blind_option_code = False
                if player.has_hand == True:
                    while True:
                        if calling_all_in == True:
                            player_bet = player.chips + player.current_bet
                            print 'ALLL INNN!!!!!!!!!!!!!'
                            pots = organize_pots.Organize_Pots().organize_pots(player_bet, player, pots, players_list)
                            break
                        player_bet = raw_input("\nYou can bet an integer,\nsay 'f' or 'fold' to fold,\nor say 'c' or 'check' to check, if the current bet is 0: ")
                        if player_bet == 'f' or player_bet == 'fold':
                            player.has_hand = False
                            player.cards = []
                            player.current_bet = 0
                            print 'You decided to fold'
                            break
                        if current_bet == 0 and player_bet == 'c' or current_bet == 0 and player_bet == 'check':
                            print ('\nYou decided to check\n')
                            player.checked = True
                            break
                        try:
                            bet = int(player_bet)
                        except ValueError:
                            print ('Enter an integer.')
                            continue
                        #if bet > current_bet and not bet >= (current_bet + (2 * current_raise)):
                        if (player.chips + player.current_bet - bet) == 0:
                            print ('ALLLLLLLL INNNNNNNN!!!!!!!!!!')
                            if bet >= required_raise:
                                old_bet = current_bet
                                raise_amount = bet - old_bet
                                pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                                current_raise = (2 * (bet - current_bet) + current_bet)
                                required_raise = ((2 * raise_amount) + old_bet)
                                print 'required raise: %d' % required_raise
                                current_bet = bet
                                print('%s chips: %d' % (player.screen_name, player.chips))
                                break
                            elif bet < required_raise:
                                old_bet = current_bet
                                pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                                current_bet = bet
                                print('%s chips: %d' % (player.screen_name, player.chips))
                                break
                        elif bet > current_bet and not bet >= required_raise:
                            print ('If you\'d like to raise, enter a number at least twice the current bet.')
                            continue
                        elif current_bet > bet:
                            print ('That is smaller than the current bet. Stop cheating, you cheapskate!!!')
                            continue
                        elif bet > player.chips:
                            print ('You don\'t have enough chips to bet that much!')
                            continue
                        elif current_bet == 0 and bet == 0:
                            print ('\nYou decided to check\n')
                            player.checked = True
                            break
                        elif bet == current_bet:
                            pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                            print ('%s chips: %d' % (player.screen_name, player.chips))
                            break
                        else:
                            old_bet = current_bet
                            raise_amount = bet - old_bet
                            #current_raise = (2 * (bet - current_bet) + current_bet)
                            required_raise = ((2 * raise_amount) + old_bet)
                            pots = organize_pots.Organize_Pots().organize_pots(bet, player, pots, players_list)
                            print 'required raise: %d' % required_raise
                            current_bet = bet
                            print('%s chips: %d' % (player.screen_name, player.chips))
                            break

                
                print "\nmade it to left_to_act code postflop\n"
                #end the game with a winner
                left_to_act = 0
                players_with_hands = 0
                all_in_players = []
                remaining_players = []
                for player in players:
                    #print player.screen_name
                    if player.has_hand == True:
                        #print 'why not go here? 2'
                        players_with_hands += 1
                        remaining_players.append(player)
                        if player.all_in == True:
                            all_in_players.append(player)
                            continue
                        if player.current_bet < current_bet:
                            left_to_act += 1
                            continue
                        if current_bet == 0:
                            if player.current_bet == 0:
                                if not player.checked:
                                    left_to_act += 1
                #print 'why not go here? 2'
                #print "players left to act: %s" % left_to_act
                #print "players with hands: %s" % players_with_hands
                if players_with_hands == 1:
                    winner = []
                    for player in players:
                        if player.has_hand == True:
                            winner.append(player)
                    #print 'Returning betting round based on 1 winner. hand_over == True'
                    return ({'hand_over': True}, pots, winner)
                if len(all_in_players) >= (players_with_hands - 1) and left_to_act == 0:
                    #print 'Returning betting round based everybody except the highest stack being all in. Left to act must == 0. hand_over == False, but errbody_all_in == True.'
                    return ({'hand_over': False}, pots, remaining_players, {'errbody_all_in': True})
                if left_to_act == 0:
                    #print 'why not go here? 3'
                    print "\nround is over\n"
                    pot_counter = 0
                    print '\nEnding of "betting_round"...'
                    #print 'Returning betting round based nobody left to act. The next rounds will have betting still. hand_over == False, errbody_all_in == False'
                    return ({'hand_over': False}, pots, remaining_players, {'errbody_all_in': False})












