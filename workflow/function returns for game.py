    @staticmethod
    def start_hand(players, button, big_blind, small_blind, deck, one_big_blind):
        return (preflop_list, pots, active_players_list)

    @staticmethod
    def run_hand(players, button, big_blind, small_blind, one_big_blind):
        return (active_players_list, was_one_big_blind)

    @staticmethod
    def betting_round(players, forced_bet, minimum_bet, current_pots, is_preflop, big_blind):
        return ({'hand_over': False}, pots)

    @staticmethod
    def run_poker_game(players, big_blind, small_blind):
        return (winner)
