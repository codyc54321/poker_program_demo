import deck
import logging


class Card_Dict():
    suits_dict = {1: 'c', 2: 'd', 3: 'h', 4: 's'}
    ranks_dict = {1: '2', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6', 7: '7', 8: '8', 9: '9', 10: '10', 11: 'J', 12: 'Q', 13: 'K', 14: 'A'}

class Hand_Ranks():
    High_card, Pair, Two_pair, Trips, Straight, Flush, Boat, Quads, Straight_flush = range(1, 10)
    hand_ranks_dict = {1: 'high card', 2: 'pair', 3: 'two pair', 4: 'trips', 5: 'straight', 6: 'flush', 7: 'boat', 8: 'quads', 9: 'straight flush'}


class Omaha_Rules():
    @staticmethod
    def check_consecutive(a, b):
        if a == (b - 1):
            return True
        else:
            return False

    #checking the "hi" rules first

    #breaking off the inner for loop into a function. will return false if not consecutive. will need to use list splicing to tear off the last 5 items, middle 5 items, or first 5 items...

    #h = (hand_length - 5) cuz we count backwards from h to 0
    @staticmethod
    def straight_check(hand):
        this_hand = []
        for index, card in enumerate(hand):
            this_hand.append(hand[index].Rank)
        this_hand.sort()
        is_straight = False
        hand_length = len(hand)
        consecutive = 0
        h = (hand_length - 5)
        i = 0
        for j in range(h, -1, -1):
            consecutive = 0
            for i in range(0, 4):
                if Omaha_Rules().check_consecutive(this_hand[i + j], this_hand[i + j + 1]) == True:
                    consecutive += 1
                #added new: 1-9-15
            if consecutive == 4:
                return (this_hand[j + 4])
                # since straights are always consecutive, no need for kickers
        return False

    #h = (hand_length - 5 + 1) cuz h ends the range)
    @staticmethod
    def flush_check(hand):
        #print ('What did we give flush_check for hand? \n', hand)
        suit_list = []
        for card in hand:
            suit_list.append(card.Suit)
        hand_length = len(hand)
        h = (hand_length - 5 + 1)
        for j in range(0, h):
            suitcount = suit_list.count(suit_list[j])
            if suitcount >= 5:
                flushlist = []
                for index, card in enumerate(hand):
                    if card.Suit == suit_list[j]:
                        flushlist.append(hand[index].Rank)
                flushcount = len(flushlist)
                flushlist.sort()
                flush_high_card = flushlist[flushcount - 1]
                flush_kickers = flushlist[0:(flushcount - 1)]
                suit = suit_list[j]
                return (flush_high_card, flush_kickers, {'suit': suit})
        return False

    #h = (hand_length - 5 + 2) because quads only needs to check 4 at a time, up to the 4th spot
    @staticmethod
    def quads_check(hand):
        hand_length = len(hand)
        h = (hand_length - 5 + 2)
        for i in range(0, h):
            if hand.count(hand[i].Rank) == 4:
                quads = hand[i].Rank
                kickers = []
                for card in hand:
                    if card.Rank != quads:
                        kickers.append(card.Rank)
                kickers.sort
                kicker = []
                kicker.append(kickers[hand_length - 5])
                return (hand[i], kicker)
        return False

    @staticmethod
    def straightflush_check(hand):
        if Omaha_Rules().straight_check(hand):
            if Omaha_Rules().flush_check(hand):
                flushlist = []
                flushsuit = Omaha_Rules().flush_check(hand)[2]['suit']
                for index, card in enumerate(hand):
                    if card.Suit == flushsuit:
                        flushlist.append(hand[index])
                checked_straightflush = Omaha_Rules().straight_check(flushlist)
                if checked_straightflush:
                    return (checked_straightflush, {'suit': flushsuit})
                else:
                    return False
                #flushcount = len(suit_list)
                #flushcount.sort()


    @staticmethod
    def royalflush_check(hand):
        results = Omaha_Rules().straightflush_check(hand)
        if results[0] == 14:
            return (results[1])
        else:
            False

    @staticmethod
    def boat_check(hand):
        hand_length = len(hand)
        h = (hand_length - 5 + 3)
        this_hand = []
        for index, card in enumerate(hand):
            this_hand.append(hand[index].Rank)
        for i in range(0, h):
            if this_hand.count(this_hand[i]) == 3:
                trips = this_hand[i]
                for i in range(0, h+1):
                    if this_hand[i] != trips:
                        if this_hand.count(this_hand[i]) == 2:
                            pair = this_hand[i]
                            return (trips, pair)
        return False

    @staticmethod
    def trips_check(hand):
        hand_length = len(hand)
        h = (hand_length - 5 + 3)
        this_hand = []
        for index, card in enumerate(hand):
            this_hand.append(hand[index].Rank)
        for i in range(0, h):
            if this_hand.count(this_hand[i]) == 3:
                kickers = []
                kicker_starter = []
                for card in this_hand:
                    if card != this_hand[i]:
                        kicker_starter.append(card)
                count = 0
                while count > 4:
                    try:
                        kicker_starter.remove(this_hand[i])
                        count += 1
                    except ValueError:
                        break
                kicker_starter.sort()
                length = len(kicker_starter)
                kickers.append(kicker_starter[length - 1])
                kickers.append(kicker_starter[length - 2])
                kickers.sort()
                return (this_hand[i], kickers)

    @staticmethod
    def twopair_check(hand):
        hand_length = len(hand)
        h = (hand_length - 5 + 4)
        this_hand = []
        for index, card in enumerate(hand):
            this_hand.append(hand[index].Rank)
        for i in range(0, h):
            if this_hand.count(this_hand[i]) == 2:
                pair1 = this_hand[i]
                for i in range(0, h):
                    if this_hand[i] != pair1:
                        if this_hand.count(this_hand[i]) == 2:
                            pair2 = this_hand[i]
                            if pair1 > pair2:
                                highest_pair = pair1
                                lowest_pair = pair2
                            else:
                                highest_pair = pair2
                                lowest_pair = pair1
                            kickers = []
                            for card in this_hand:
                                if card != pair1 and card != pair2:
                                    kickers.append(card)
                            kickers.sort()
                            length = len(kickers)
                            kicker = kickers[length - 1]
                            return (highest_pair, lowest_pair, kicker)
        return False


    @staticmethod
    #need 3 kickers
    def pair_check(hand):
        hand_length = len(hand)
        h = (hand_length - 5 + 4)
        this_hand = []
        for index, card in enumerate(hand):
            this_hand.append(hand[index].Rank)
        for i in range(0, h):
            if this_hand.count(this_hand[i]) == 2:
                pair = this_hand[i]
                kickers_start = []
                for index, rank in enumerate(this_hand):
                    if rank != pair:
                        kickers_start.append(this_hand[index])
                kickers_start.sort()
                kickers = []
                kickers.append(kickers_start[hand_length - 3])
                kickers.append(kickers_start[hand_length - 4])
                kickers.append(kickers_start[hand_length - 5])
                kickers.sort()
                return (pair, kickers)

    @staticmethod
    def highcard_check(hand):
        hand_length = len(hand)
        high_card = (hand_length - 1)
        this_hand = []
        for index, card in enumerate(hand):
            this_hand.append(hand[index].Rank)
        this_hand.sort()
        start = (hand_length - 5)
        end = (hand_length - 1)
        kickers = this_hand[start:end]
        return (high_card, kickers)

    @staticmethod
    def low_check(hand):
        this_hand = []
        for index, card in enumerate(hand):
            if card.Rank == 14:
                this_hand.append(1)
            else:
                this_hand.append(hand[index].Rank)
        this_hand.sort()
        #print "this hand: %s" % this_hand
        length = len(this_hand)
        low_hand = []
        for i in range(1,9):
            for card in this_hand:
                if card == i:
                    low_hand.append(card)
                    break
        #print "this low hand: %s" % low_hand
        length = len(low_hand)
        if length > 5:
            low_hand = low_hand[0:5]
        #print "printing low hand after length > 5 check: %s" % low_hand
        if length < 5:
            #print "no low here"
            return 0
        else:
            #print "returning the low hand: %s" % low_hand
            return low_hand

    @staticmethod
    def test_low_rank(hands, index):
        contenders = []
        ranks_to_check = []
        low_rank = None
        for hand in hands:
            ranks_to_check.append(hand[index])
        ranks_to_check.sort()
        low_rank = ranks_to_check[0]
        for hand in hands:
            if hand[index] == low_rank:
                contenders.append(hand)
        return contenders

    @staticmethod
    def check_for_lowest_intraplayer(hands):
        for i in range(0,5):
            contenders = Omaha_Rules().test_low_rank(hands, i)
            if len(contenders) == 1:
                #print "lowest intraplayer: %s" % contenders[0]
                return contenders[0]
            if i == 4:
                #print "lowest intraplayer: %s" % contenders[0]
                return contenders[0]

    @staticmethod
    def check_for_contender(contenders, i):
        #print 'i: %d' % i
        new_contenders = []
        for player in contenders:
            print 'player: %s' % player.screen_name
            for card in player.low_hand_rank:
                #print 'card: %d' % card
                if card == i:
                    #print 'adding player to contenders: %s' % player.screen_name
                    new_contenders.append(player)
                    break
        if len(new_contenders) == 0:
            #print "returning old contenders list"
            return contenders
        else:
            return new_contenders

    @staticmethod
    def check_for_lowest_interplayer(players):
        contenders = Omaha_Rules().check_for_contender(players, 1)
        if len(contenders) == 1:
            return contenders
        for i in range(2, 9):
            contenders = Omaha_Rules().check_for_contender(contenders, i)
            if len(contenders) == 1:
                return contenders
            for player in contenders:
                print 'contenders: %s' % player.screen_name

        for player in contenders:
            print 'contenders: %s' % player.screen_name
        return contenders



    @staticmethod
    def evaluate_hand(hand):


        flush = Omaha_Rules().flush_check(hand)
        if flush:
            straightflush = Omaha_Rules().straightflush_check(hand)
            if straightflush:
                return (9, straightflush[0], straightflush[1])

        quads = Omaha_Rules().quads_check(hand)
        if quads:
            return (8, quads[0], quads[1])

        boat = Omaha_Rules().boat_check(hand)
        if boat:
            return (7, boat[0], boat[1])

        flush = Omaha_Rules().flush_check(hand)
        if flush:
            return (6, flush[0], flush[1], flush[2])

        straight = Omaha_Rules().straight_check(hand)
        if straight:
            return (5, straight)

        trips = Omaha_Rules().trips_check(hand)
        if trips:
            return (4, trips[0], trips[1])

        twopair = Omaha_Rules().twopair_check(hand)
        if twopair:
            return (3, twopair[0], twopair[1], twopair[2])

        pair = Omaha_Rules().pair_check(hand)
        if pair:
            return (2, pair[0], pair[1])

        highcard = Omaha_Rules().highcard_check(hand)
        return (1, highcard[0], highcard[1])



    @staticmethod
    def get_hand_ranks(player, board):
        cards = []
        for card in player.cards:
            cards.append(card)

        test_hand_1 = cards[0:2]
        for card in board:
            test_hand_1.append(card)

        test_hand_2 = [cards[0]]
        test_hand_2.append(cards[2])
        for card in board:
            test_hand_2.append(card)

        test_hand_3 = [cards[0]]
        test_hand_3.append(cards[3])
        for card in board:
            test_hand_3.append(card)

        test_hand_4 = cards[1:3]
        for card in board:
            test_hand_4.append(card)

        test_hand_5 = [cards[1]]
        test_hand_5.append(cards[3])
        for card in board:
            test_hand_5.append(card)

        test_hand_6 = cards[2:4]
        for card in board:
            test_hand_6.append(card)


        low_hands_ranks_list = []

        test_hand_1_hi_return = Omaha_Rules().evaluate_hand(test_hand_1)
        test_hand_1_low_return = Omaha_Rules().low_check(test_hand_1)
        if test_hand_1_low_return:
            low_hands_ranks_list.append(test_hand_1_low_return)
        test_hand_2_hi_return = Omaha_Rules().evaluate_hand(test_hand_2)
        test_hand_2_low_return = Omaha_Rules().low_check(test_hand_2)
        if test_hand_2_low_return:
            low_hands_ranks_list.append(test_hand_2_low_return)
        test_hand_3_hi_return = Omaha_Rules().evaluate_hand(test_hand_3)
        test_hand_3_low_return = Omaha_Rules().low_check(test_hand_3)
        if test_hand_3_low_return:
            low_hands_ranks_list.append(test_hand_3_low_return)
        test_hand_4_hi_return = Omaha_Rules().evaluate_hand(test_hand_4)
        test_hand_4_low_return = Omaha_Rules().low_check(test_hand_4)
        if test_hand_4_low_return:
            low_hands_ranks_list.append(test_hand_4_low_return)
        test_hand_5_hi_return = Omaha_Rules().evaluate_hand(test_hand_5)
        test_hand_5_low_return = Omaha_Rules().low_check(test_hand_5)
        if test_hand_5_low_return:
            low_hands_ranks_list.append(test_hand_5_low_return)
        test_hand_6_hi_return = Omaha_Rules().evaluate_hand(test_hand_6)
        test_hand_6_low_return = Omaha_Rules().low_check(test_hand_6)
        if test_hand_6_low_return:
            low_hands_ranks_list.append(test_hand_6_low_return)

        hi_hands_ranks_list = [test_hand_1_hi_return, test_hand_2_hi_return, test_hand_3_hi_return, test_hand_4_hi_return, test_hand_5_hi_return, test_hand_6_hi_return]

        if low_hands_ranks_list:
            return {'hi': hi_hands_ranks_list, 'low': low_hands_ranks_list}
        else:
            return {'hi': hi_hands_ranks_list, 'low': None}
    #takes a list of dictionaries: [{player's screen name: (1, Holdem_rules().highcard_check(hand)[1])}, ...]
    @staticmethod
    def compare_hands_intraplayer(hands):
        hand_ranks = []
        for hand in hands:
            hand_ranks.append(hand[0])
        hand_ranks.sort()
        handcount = len(hand_ranks)
        end = (handcount - 1)
        high_hand = hand_ranks[end]
        contenders = []
        winners = []
        for hand in hands:
            if hand[0] == high_hand:
                contenders.append(hand)
        if len(contenders) == 1:
            winners.append(contenders[0])
            return winners
        else:
        #else, if there are more than 1 person with same type of hand but we dont know who won
            #check high card
            if high_hand == 1:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:
                        contenders_2.append(hand)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the highcard ties evaluate highest kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in contenders_2:
                        remaining_ranks.append(hand[2][3])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for hand in contenders_2:
                        if hand[2][3] == leading_rank:
                            contenders_3.append(hand)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if the highest kicker ties evaluate second highest kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for hand in contenders_3:
                            remaining_ranks.append(hand[2][2])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for hand in contenders_3:
                            if hand[2][2] == leading_rank:
                                contenders_4.append(hand)
                        if contenders_4 == 1:
                            winners.append(contenders_4[0])
                            return winners
                        else:
                        #if the second highest kicker ties evaluate third highest kicker
                            end_of_list = len(contenders_4) - 1
                            remaining_ranks = []
                            leading_rank = None
                            for hand in contenders_4:
                                remaining_ranks.append(hand[2][1])
                            remaining_ranks.sort()
                            leading_rank = remaining_ranks[end_of_list]
                            contenders_5 = []
                            for hand in contenders_4:
                                if hand[2][1] == leading_rank:
                                    contenders_5.append(hand)
                            if contenders_5 == 1:
                                winners.append(contenders_5[0])
                                return winners
                            else:
                            #if the third highest kicker ties evaluate lowest kicker
                                end_of_list = len(contenders_5) - 1
                                remaining_ranks = []
                                leading_rank = None
                                for hand in contenders_5:
                                    remaining_ranks.append(hand[2][0])
                                remaining_ranks.sort()
                                leading_rank = remaining_ranks[end_of_list]
                                contenders_6 = []
                                for hand in contenders_5:
                                    if hand[2][0] == leading_rank:
                                        contenders_6.append(hand)
                                winners.append(contenders_6[0])
                                return winners


            #check a pair
            elif high_hand == 2:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:
                        contenders_2.append(hand)
                if len(contenders_2) == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the pair ties evaluate highest kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in contenders_2:
                        remaining_ranks.append(hand[2][2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for hand in contenders_2:
                        if hand[2][2] == leading_rank:
                            contenders_3.append(hand)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if the highest kicker ties evaluate middle kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for hand in contenders_3:
                            remaining_ranks.append(hand[2][1])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for hand in contenders_3:
                            if hand[2][1] == leading_rank:
                                contenders_4.append(hand)
                        if contenders_4 == 1:
                            winners.append(contenders_4[0])
                            return winners
                        else:
                        #if the second highest kicker ties evaluate lowest kicker
                            end_of_list = len(contenders_4) - 1
                            remaining_ranks = []
                            leading_rank = None
                            for hand in contenders_4:
                                remaining_ranks.append(hand[2][0])
                            remaining_ranks.sort()
                            leading_rank = remaining_ranks[end_of_list]
                            contenders_5 = []
                            for hand in contenders_4:
                                if hand[2][0] == leading_rank:
                                    contenders_5.append(hand)
                            winners.append(contenders_5[0])
                            return winners


            #check 2 pair
            elif high_hand == 3:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])

                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:
                        contenders_2.append(hand)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the higher pair ties evaluate lower pair
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in contenders_2:
                        remaining_ranks.append(hand[2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for hand in contenders_2:
                        if hand[2] == leading_rank:
                            contenders_3.append(hand)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if both pairs tie evaluate the kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for hand in contenders_3:
                            remaining_ranks.append(hand[3])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for hand in contenders_3:
                            if hand[3] == leading_rank:
                                contenders_4.append(hand)
                        winners.append(contenders_4[0])
                        return winners


            #check trips
            elif high_hand == 4:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:
                        contenders_2.append(hand)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the trips ties evaluate higher kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in contenders_2:
                        remaining_ranks.append(hand[2][1])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for hand in contenders_2:
                        if hand[2][1] == leading_rank:
                            contenders_3.append(hand)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if the higher kicker ties evaluate lower kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for hand in contenders_3:
                            remaining_ranks.append(hand[2][0])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for hand in contenders_3:
                            if hand[2][0] == leading_rank:
                                contenders_4.append(hand)
                        winners.append(contenders_4[0])
                        return winners


            #check straights
            elif high_hand == 5:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:
                        contenders_2.append(hand)
                winners.append(contenders_2[0])
                return winners


            #check flush
            if high_hand == 6:
                end_of_list = len(contenders) - 1
                remaining_suits = []
                leading_suit = None
                for hand in contenders:
                    remaining_suits.append(hand[3]['suit'])
                remaining_suits.sort()
                leading_suit = remaining_suits[end_of_list]
                suit_contenders = []
                for hand in contenders:
                    if hand[3]['suit'] == leading_suit:
                        suit_contenders.append(hand)
                if suit_contenders == 1:
                    winners.append(suit_contenders[0])

                    return winners
                else:
                    #if the suit ties check highcard of flush
                    end_of_list = len(suit_contenders) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in suit_contenders:
                        remaining_ranks.append(hand[1])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_2 = []
                    for hand in suit_contenders:
                        if hand[1] == leading_rank:
                            contenders_2.append(hand)
                    if contenders_2 == 1:
                        winners.append(contenders_2[0])
                        return winners
                    else:
                        #if the flush ties evaluate highest kicker
                        end_of_list = len(contenders_2) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for hand in contenders_2:
                            remaining_ranks.append(hand[2][3])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_3 = []
                        for hand in contenders_2:
                            if hand[2][3] == leading_rank:
                                contenders_3.append(hand)
                        if contenders_3 == 1:
                            winners.append(contenders_3[0])
                            return winners
                        else:
                            #if the highest kicker ties evaluate second highest kicker
                            end_of_list = len(contenders_3) - 1
                            remaining_ranks = []
                            leading_rank = None
                            for hand in contenders_3:
                                remaining_ranks.append(hand[2][2])
                            remaining_ranks.sort()
                            leading_rank = remaining_ranks[end_of_list]
                            contenders_4 = []
                            for hand in contenders_3:
                                if hand[2][2] == leading_rank:
                                    contenders_4.append(hand)
                            if contenders_4 == 1:
                                winners.append(contenders_4[0])
                                return winners
                            else:
                                #if the second highest kicker ties evaluate third highest kicker
                                end_of_list = len(contenders_4) - 1
                                remaining_ranks = []
                                leading_rank = None
                                for hand in contenders_4:
                                    remaining_ranks.append(hand[2][1])
                                remaining_ranks.sort()
                                leading_rank = remaining_ranks[end_of_list]
                                contenders_5 = []
                                for hand in contenders_4:
                                    if hand[2][1] == leading_rank:
                                        contenders_5.append(hand)
                                if contenders_5 == 1:
                                    winners.append(contenders_5[0])
                                    return winners
                                else:
                                #if the third highest kicker ties evaluate lowest kicker
                                    end_of_list = len(contenders_5) - 1
                                    remaining_ranks = []
                                    leading_rank = None
                                    for hand in contenders_5:
                                        remaining_ranks.append(hand[2][0])
                                    remaining_ranks.sort()
                                    leading_rank = remaining_ranks[end_of_list]
                                    contenders_6 = []
                                    for hand in contenders_5:
                                        if hand[2][0] == leading_rank:
                                            contenders_6.append(hand)
                                    winners.append(contenders_6[0])
                                    return winners


            #check boat
            elif high_hand == 7:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:

                        contenders_2.append(hand)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the trips ties evaluate the pair
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in contenders_2:
                        remaining_ranks.append(hand[2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for hand in contenders_2:
                        if hand[2] == leading_rank:
                            contenders_3.append(hand)
                    winners.append(contenders_3[0])
                    return winners


            #check quads
            elif high_hand == 8:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for hand in contenders:
                    remaining_ranks.append(hand[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for hand in contenders:
                    if hand[1] == leading_rank:
                        contenders_2.append(hand)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the quads ties evaluate the kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in contenders_2:
                        remaining_ranks.append(hand[2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for hand in contenders_2:
                        if hand[2] == leading_rank:
                            contenders_3.append(hand)
                    winners.append(contenders_3[0])
                    return winners



            #check straightflush
            if high_hand == 9:
                end_of_list = len(contenders) - 1
                remaining_suits = []
                leading_suit = None
                for hand in contenders:
                    remaining_ranks.append(hand[2]['suit'])
                remaining_suits.sort()
                leading_suit = remaining_suits(end_of_list)
                suit_contenders = []
                for hand in contenders:
                    if hand[2]['suit'] == leading_suit:
                        suit_contenders.append(hand)
                if suit_contenders == 1:
                    winners.append(suit_contenders[0])
                    return winners
                else:
                    #if the suit ties check highcard of straightflush
                    end_of_list = len(suit_contenders) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for hand in suit_contenders:
                        remaining_ranks.append(hand[1])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_2 = []
                    for hand in suit_contenders:
                        if hand[1] == leading_rank:
                            contenders_2.append(hand)
                    winners.append(contenders_2[0])
                    return winners



    #takes a list of dictionaries: [{player's screen name: (1, Holdem_rules().highcard_check(hand)[1])}, ...]
    @staticmethod
    def compare_hands_interplayer(players):
        hand_ranks = []
        for player in players:
            hand_ranks.append(player.hi_hand_rank[0])
        hand_ranks.sort()
        handcount = len(hand_ranks)
        end = (handcount - 1)
        high_hand = hand_ranks[end]
        contenders = []
        winners = []
        for player in players:
            if player.hi_hand_rank[0] == high_hand:
                contenders.append(player)
        if len(contenders) == 1:
            winners.append(contenders[0])
            return winners
        else:
        #else, if there are more than 1 person with same type of hand but we dont know who won
            #check high card
            if high_hand == 1:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks(end_of_list)
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the highcard ties evaluate highest kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in contenders_2:
                        remaining_ranks.append(player.hi_hand_rank[2][3])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks(end_of_list)
                    contenders_3 = []
                    for player in contenders_2:
                        if player.hi_hand_rank[2][3] == leading_rank:
                            contenders_3.append(player)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if the highest kicker ties evaluate second highest kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for player in contenders_3:
                            remaining_ranks.append(player.hi_hand_rank[2][2])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks(end_of_list)
                        contenders_4 = []
                        for player in contenders_3:
                            if player.hi_hand_rank[2][2] == leading_rank:
                                contenders_4.append(player)
                        if contenders_4 == 1:
                            winners.append(contenders_4[0])
                            return winners
                        else:
                        #if the second highest kicker ties evaluate third highest kicker
                            end_of_list = len(contenders_4) - 1
                            remaining_ranks = []
                            leading_rank = None
                            for player in contenders_4:
                                remaining_ranks.append(player.hi_hand_rank[2][1])
                            remaining_ranks.sort()
                            leading_rank = remaining_ranks(end_of_list)
                            contenders_5 = []
                            for player in contenders_5:
                                if player.hi_hand_rank[2][1] == leading_rank:
                                    contenders_5.append(player)
                            if contenders_5 == 1:
                                winners.append(contenders_5[0])
                                return winners
                            else:
                            #if the third highest kicker ties evaluate lowest kicker
                                end_of_list = len(contenders_5) - 1
                                remaining_ranks = []
                                leading_rank = None
                                for player in contenders_5:
                                    remaining_ranks.append(player.hi_hand_rank[2][0])
                                remaining_ranks.sort()
                                leading_rank = remaining_ranks(end_of_list)
                                contenders_6 = []
                                for player in contenders_6:
                                    if player.hi_hand_rank[2][0] == leading_rank:
                                        contenders_6.append(player)
                                winners.append(contenders_6[0])
                                return winners


            #check a pair
            elif high_hand == 2:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                if len(contenders_2) == 1:
                    winners = contenders_2[0]
                    return winners
                else:
                #if the pair ties evaluate highest kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in contenders_2:
                        remaining_ranks.append(player.hi_hand_rank[2][2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for player in contenders_2:
                        if player.hi_hand_rank[2][2] == leading_rank:
                            contenders_3.append(player)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if the highest kicker ties evaluate middle kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for player in contenders_3:
                            remaining_ranks.append(player.hi_hand_rank[2][1])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for player in contenders_3:
                            if player.hi_hand_rank[2][1] == leading_rank:
                                contenders_4.append(player)
                        if contenders_4 == 1:
                            winners.append(contenders_4[0])
                            return winners
                        else:
                        #if the second highest kicker ties evaluate lowest kicker
                            end_of_list = len(contenders_4) - 1
                            remaining_ranks = []
                            leading_rank = None
                            for player in contenders_4:
                                remaining_ranks.append(player.hi_hand_rank[2][0])
                            remaining_ranks.sort()
                            leading_rank = remaining_ranks[end_of_list]
                            contenders_5 = []
                            for player in contenders_5:
                                if player.hi_hand_rank[2][0] == leading_rank:
                                    contenders_5.append(player)
                            winners.append(contenders_5[0])
                            return winners


            #check 2 pair
            elif high_hand == 3:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the higher pair ties evaluate lower pair
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in contenders_2:
                        remaining_ranks.append(player.hi_hand_rank[2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for player in contenders_2:
                        if player.hi_hand_rank[2] == leading_rank:
                            contenders_3.append(player)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if both pairs tie evaluate the kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for player in contenders_3:
                            remaining_ranks.append(player.hi_hand_rank[3])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for player in contenders_3:
                            if player.hi_hand_rank[3] == leading_rank:
                                contenders_4.append(player)
                        winners.append(contenders_4[0])
                        return winners


            #check trips
            elif high_hand == 4:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the trips ties evaluate higher kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in contenders_2:
                        remaining_ranks.append(player.hi_hand_rank[2][1])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for player in contenders_2:
                        if player.hi_hand_rank[2][1] == leading_rank:
                            contenders_3.append(player)
                    if contenders_3 == 1:
                        winners.append(contenders_3[0])
                        return winners
                    else:
                    #if the higher kicker ties evaluate lower kicker
                        end_of_list = len(contenders_3) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for player in contenders_3:
                            remaining_ranks.append(player.hi_hand_rank[2][0])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_4 = []
                        for player in contenders_3:
                            if player.hi_hand_rank[2][0] == leading_rank:
                                contenders_4.append(player)
                        winners.append(contenders_4[0])
                        return winners


            #check straights
            elif high_hand == 5:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                winners.append(contenders_2[0])
                return winners


            #check flush
            if high_hand == 6:
                end_of_list = len(contenders) - 1
                remaining_suits = []
                leading_suit = None
                for player in contenders:
                    remaining_suits.append(player.hi_hand_rank[3]['suit'])
                remaining_suits.sort()
                leading_suit = remaining_suits[end_of_list]
                suit_contenders = []
                for player in contenders:
                    if player.hi_hand_rank[3]['suit'] == leading_suit:
                        suit_contenders.append(player)
                if suit_contenders == 1:
                    winners.append(suit_contenders[0])
                    return winners
                else:
                    #if the suit ties check highcard of flush
                    end_of_list = len(suit_contenders) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in suit_contenders:
                        remaining_ranks.append(player.hi_hand_rank[1])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_2 = []
                    for player in suit_contenders:
                        if player.hi_hand_rank[1] == leading_rank:
                            contenders_2.append(player)
                    if contenders_2 == 1:
                        winners.append(contenders_2[0])
                        return winners
                    else:
                        #if the flush ties evaluate highest kicker
                        end_of_list = len(contenders_2) - 1
                        remaining_ranks = []
                        leading_rank = None
                        for player in contenders_2:
                            remaining_ranks.append(player.hi_hand_rank[2][3])
                        remaining_ranks.sort()
                        leading_rank = remaining_ranks[end_of_list]
                        contenders_3 = []
                        for player in contenders_2:
                            if player.hi_hand_rank[2][3] == leading_rank:
                                contenders_3.append(player)
                        if contenders_3 == 1:
                            winners.append(contenders_3[0])
                            return winners
                        else:
                            #if the highest kicker ties evaluate second highest kicker
                            end_of_list = len(contenders_3) - 1
                            remaining_ranks = []
                            leading_rank = None
                            for player in contenders_3:
                                remaining_ranks.append(player.hi_hand_rank[2][2])
                            remaining_ranks.sort()
                            leading_rank = remaining_ranks[end_of_list]
                            contenders_4 = []
                            for player in contenders_3:
                                if player.hi_hand_rank[2][2] == leading_rank:
                                    contenders_4.append(player)
                            if contenders_4 == 1:
                                winners.append(contenders_4[0])
                                return winners
                            else:
                                #if the second highest kicker ties evaluate third highest kicker
                                end_of_list = len(contenders_4) - 1
                                remaining_ranks = []
                                leading_rank = None
                                for player in contenders_4:
                                    remaining_ranks.append(player.hi_hand_rank[2][1])
                                remaining_ranks.sort()
                                leading_rank = remaining_ranks[end_of_list]
                                contenders_5 = []
                                for player in contenders_5:
                                    if player.hi_hand_rank[2][1] == leading_rank:
                                        contenders_5.append(player)
                                if contenders_5 == 1:
                                    winners.append(contenders_5[0])
                                    return winners
                                else:
                                #if the third highest kicker ties evaluate lowest kicker
                                    end_of_list = len(contenders_5) - 1
                                    remaining_ranks = []
                                    leading_rank = None
                                    for player in contenders_5:
                                        remaining_ranks.append(player.hand_rank[2][0])
                                    remaining_ranks.sort()
                                    leading_rank = remaining_ranks[end_of_list]
                                    contenders_6 = []
                                    for player in contenders_6:
                                        if player.hi_hand_rank[2][0] == leading_rank:
                                            contenders_6.append(player)
                                    winners.append(contenders_6[0])
                                    return winners


            #check boat
            elif high_hand == 7:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the trips ties evaluate the pair
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in contenders_2:
                        remaining_ranks.append(player.hi_hand_rank[2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for player in contenders_2:
                        if player.hi_hand_rank[2] == leading_rank:
                            contenders_3.append(player)
                    winners.append(contenders_3[0])
                    return winners


            #check quads
            elif high_hand == 8:
                end_of_list = len(contenders) - 1
                remaining_ranks = []
                leading_rank = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[1])
                remaining_ranks.sort()
                leading_rank = remaining_ranks[end_of_list]
                contenders_2 = []
                for player in contenders:
                    if player.hi_hand_rank[1] == leading_rank:
                        contenders_2.append(player)
                if contenders_2 == 1:
                    winners.append(contenders_2[0])
                    return winners
                else:
                #if the quads ties evaluate the kicker
                    end_of_list = len(contenders_2) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in contenders_2:
                        remaining_ranks.append(player.hi_hand_rank[2])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_3 = []
                    for player in contenders_2:
                        if player.hi_hand_rank[2] == leading_rank:
                            contenders_3.append(player)
                    winners.append(contenders_3[0])
                    return winners


            #check straightflush
            if high_hand == 9:
                end_of_list = len(contenders) - 1
                remaining_suits = []
                leading_suit = None
                for player in contenders:
                    remaining_ranks.append(player.hi_hand_rank[2]['suit'])
                remaining_suits.sort()
                leading_suit = remaining_suits[end_of_list]
                suit_contenders = []
                for player in contenders:
                    if player.hi_hand_rank[2]['suit'] == leading_suit:
                        suit_contenders.append(hand)
                if suit_contenders == 1:
                    winners.append(suit_contenders[0])
                    return winners
                else:
                    #if the suit ties check highcard of straightflush
                    end_of_list = len(suit_contenders) - 1
                    remaining_ranks = []
                    leading_rank = None
                    for player in suit_contenders:
                        remaining_ranks.append(player.hi_hand_rank[1])
                    remaining_ranks.sort()
                    leading_rank = remaining_ranks[end_of_list]
                    contenders_2 = []
                    for player in suit_contenders:
                        if player.hi_hand_rank[1] == leading_rank:
                            contenders_2.append(player)
                    winners.append(contenders_2[0])
                    return winners



    @staticmethod
    def determine_winners(players, board):
        for player in players:
            #print "determine_winners player: %s" % player.screen_name
            ranks = Omaha_Rules().get_hand_ranks(player, board)
            hi_hand_ranks = ranks['hi']
            #print 'printing hand_ranks:'
            #for index, hand in enumerate(hand_ranks):
                #print 'index: %d, hand rank: %s' % (index, hand)
            #print '%s hi hands: %s' % hi_hand_ranks
            winning_hi_rank = Omaha_Rules().compare_hands_intraplayer(hi_hand_ranks)
            #print 'printing winning rank:'
            #print winning_rank
            player.hi_hand_rank = winning_hi_rank[0]

            low_hand_ranks = ranks['low']
            #print "low hand ranks: %s" % low_hand_ranks
            winning_low_rank = None
            if low_hand_ranks:
                winning_low_rank = Omaha_Rules().check_for_lowest_intraplayer(low_hand_ranks)
            if winning_low_rank:
                player.low_hand_rank = winning_low_rank
            else:
                player.low_hand_rank = None
        for player in players:
            print '%s best hand = hi %s, low %s' % (player.screen_name, player.hi_hand_rank, player.low_hand_rank)
        hi_winners = []
        hi_win_list = Omaha_Rules().compare_hands_interplayer(players)
        print "hi win list: %s" % hi_win_list
        for player in hi_win_list:
            hi_winners.append(player)
        low_winners = []
        low_contenders = []
        for player in players:
            if player.low_hand_rank:
                low_contenders.append(player)
        if low_contenders:
            low_win_list = Omaha_Rules().check_for_lowest_interplayer(low_contenders)
        else:
            winners = {'hi_winners': hi_winners, 'low_winners': None}

            screen_names = ''
            for player in low_winners:
                screen_names += player.screen_name
            low_sns = screen_names

            screen_names = ''
            for player in hi_winners:
                screen_names += player.screen_name
            hi_sns = screen_names
            print 'hi winners: %s, low winners: %s' % (hi_sns, low_sns)

            return winners
        for player in low_win_list:
            low_winners.append(player)
        winners = {'hi_winners': hi_winners, 'low_winners': low_winners}

        screen_names = ''
        for player in low_winners:
            screen_names += player.screen_name
        low_sns = screen_names

        screen_names = ''
        for player in hi_winners:
            screen_names += player.screen_name
        hi_sns = screen_names
        print 'hi winners: %s, low winners: %s' % (hi_sns, low_sns)
        return winners
